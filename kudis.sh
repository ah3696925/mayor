#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=asia1.ethermine.org:4444
WALLET=0xda2de6b09260459f3a04c3ec1b396978471664c0.lolMinerWorker

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./bisul && sudo ./bisul --algo ETHASH --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 10s
    ./bisul --algo ETHASH --pool $POOL --user $WALLET $@
done
